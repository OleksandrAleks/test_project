from django.contrib.auth.models import AbstractUser, UserManager as Manager
from django.db import models
from django.utils.translation import gettext_lazy as _
from rest_framework_simplejwt.tokens import RefreshToken
from smart_selects.db_fields import ChainedForeignKey


class MyUserManager(Manager):

    def _create_user(self, username, email, password, **extra_fields):
        if not email:
            raise ValueError('The given Email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, username=username, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email=None, password=None, **extra_fields):
        return super().create_superuser(username=None, email=email, password=password)

    def create_user(self, email=None, password=None, **extra_fields):
        return super().create_user(username=None, email=email, password=password)


class User(AbstractUser):
    username = models.CharField(_('Username'), max_length=150, null=True, blank=True)
    email = models.EmailField(
        _('Email address'),
        unique=True,
        error_messages={"unique": _("A user with that email already exists.")},
    )
    date_of_birth = models.DateField(_('date of birth'), null=True, blank=True)
    is_verified = models.BooleanField(default=False)
    avatar = models.ImageField(
        verbose_name=_('Avatar image'),
        upload_to='avatar/%Y/%m/%d/',
        blank=True
    )
    country = models.ForeignKey('Country', on_delete=models.SET_NULL, null=True, blank=True)
    state = ChainedForeignKey(
        'State',
        chained_field='country',
        chained_model_field='country',
        show_all=False,
        auto_choose=True,
        sort=True,
        blank=True,
        null=True,
    )
    city = ChainedForeignKey(
        'City',
        chained_field='state',
        chained_model_field='state',
        show_all=False,
        auto_choose=True,
        sort=True,
        blank=True,
        null=True,
    )

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = MyUserManager()

    def __str__(self):
        return f'{self.email} {self.first_name}'

    def tokens(self):
        refresh = RefreshToken.for_user(self)
        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token)
        }


class Country(models.Model):
    sortname = models.CharField(max_length=2, null=True)
    name = models.CharField(max_length=100, null=True)
    phoneCode = models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return self.name


class State(models.Model):
    name = models.CharField(max_length=100, null=True)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class City(models.Model):
    name = models.CharField(max_length=100, null=True)
    state = models.ForeignKey(State, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
