from django.urls import path, include
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from rest_framework import routers
from .views import (
    LoginAPIView, LogoutAPIView, RegistrationAPIView, UpdateUserInfo, VerifyEmail, ResetUserPassword,
    ConfirmResetUserPassword, CityListView, CountryListView, StateListView, ChangeUserPassword, ChangeUserEmail
)

app_name = 'user'

router = routers.SimpleRouter()
router.register(r'auth/profile', UpdateUserInfo, basename='profile')
router.register(r'auth/cities', CityListView, basename='cities')
router.register(r'auth/countries', CountryListView, basename='countries')
router.register(r'auth/states', StateListView, basename='states')

urlpatterns = [
    path('auth/register/', RegistrationAPIView.as_view(), name='register'),
    path('auth/email-verify/', VerifyEmail.as_view(), name='email_verify'),
    path('auth/login/', LoginAPIView.as_view(), name='login'),
    path('auth/logout/', LogoutAPIView.as_view(), name='logout'),
    path('auth/request-password-reset/', ResetUserPassword.as_view(), name='request_password_reset'),
    path('password-reset/<str:encoded_pk>/<str:token>/', ConfirmResetUserPassword.as_view(), name='reset_password'),
    path('auth/password-change/', ChangeUserPassword.as_view(), name='change_password'),
    path('auth/change-email/', ChangeUserEmail.as_view(), name='change_email'),
    path('', include(router.urls)),
    path('auth/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('auth/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]
