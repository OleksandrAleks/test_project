import jwt
from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from rest_framework import filters, permissions, status
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, UpdateModelMixin
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from app import settings
from .models import City, Country, State
from .serializers import (
    ChangePassSerializer, ChangeUserEmailSerializer, CitySerializer, ConfirmResetUserPasswordSerializer,
    CountrySerializer, EmailVerificationSerializer, LoginSerializer, LogoutSerializer, RegistrationSerializer,
    ResetUserPasswordSerializer, StateSerializer, UpdateUserInfoSerializer
)
from .tasks import send_email_for_change_password, send_email_for_verification

User = get_user_model()


class RegistrationAPIView(GenericAPIView):
    queryset = User.objects.all()
    serializer_class = RegistrationSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            data = serializer.data
            email = data.get('email')
            site_domain = settings.SITE_DOMAIN
            send_email_for_verification.delay(email, site_domain)
            return Response({
                "Message": "User created successfully. Check your email",
                "User": serializer.data}, status=status.HTTP_201_CREATED
            )
        return Response({"Errors": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


class VerifyEmail(APIView):
    serializer_class = EmailVerificationSerializer

    def get(self, request):
        token = request.GET.get('token')
        try:
            token_payload = jwt.decode(token, settings.SECRET_KEY, algorithms=['HS256'])
            user = User.objects.get(id=token_payload['user_id'])
            if not user.is_verified:
                user.is_verified = True
                user.save()
            return Response({'email': 'Successfully activated'}, status=status.HTTP_200_OK)
        except jwt.ExpiredSignatureError as identifier:
            return Response({'error': 'Activation Expired'}, status=status.HTTP_400_BAD_REQUEST)
        except jwt.exceptions.DecodeError as identifier:
            return Response({'error': 'Invalid token'}, status=status.HTTP_400_BAD_REQUEST)


class LoginAPIView(GenericAPIView):
    # serializer_class = TokenObtainPairSerializer
    serializer_class = LoginSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class LogoutAPIView(GenericAPIView):
    serializer_class = LogoutSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class UpdateUserInfo(RetrieveModelMixin, UpdateModelMixin, GenericViewSet):
    serializer_class = UpdateUserInfoSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        user = self.request.user
        return User.objects.filter(pk=user.id)


class ResetUserPassword(GenericAPIView):
    serializer_class = ResetUserPasswordSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        email = serializer.data.get('email', '')
        if User.objects.filter(email=email).exists():
            user = User.objects.get(email=email)
            encoded_pk = urlsafe_base64_encode(force_bytes(user.pk))
            token = PasswordResetTokenGenerator().make_token(user)
            site_domain = settings.SITE_DOMAIN
            send_email_for_change_password.delay(encoded_pk, token, site_domain, email)
            return Response({"Message": f"Your password reset link send to your email."}, status=status.HTTP_200_OK)

        else:
            return Response({"Message": "User doesn't exists"}, status=status.HTTP_400_BAD_REQUEST)


class ConfirmResetUserPassword(GenericAPIView):
    serializer_class = ConfirmResetUserPasswordSerializer

    def patch(self, request, *args, **kwargs):

        serializer = self.serializer_class(data=request.data, context={"kwargs": kwargs})
        serializer.is_valid(raise_exception=True)
        return Response({"Message": "Password reset complete"}, status=status.HTTP_200_OK)


class ChangeUserPassword(GenericAPIView):
    serializer_class = ChangePassSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def patch(self, request):
        user = self.request.user
        serializer = self.serializer_class(user, data=request.data)
        if serializer.is_valid():
            user.set_password(self.request.data.get('password'))
            user.save()
            return Response({"Message": "Password change complete"}, status=status.HTTP_200_OK)
        return Response({"Errors": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


class ChangeUserEmail(GenericAPIView):
    serializer_class = ChangeUserEmailSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def patch(self, request):
        user = self.request.user
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            email = serializer.data.get('email')
            user.email = email
            user.save()
            return Response({"Message": f"Your email wos change on {serializer.data}"}, status=status.HTTP_200_OK)
        return Response({"Errors": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


class CountryListView(ListModelMixin, GenericViewSet):
    serializer_class = CountrySerializer
    queryset = Country.objects.all()
    filter_backends = [filters.SearchFilter]
    search_fields = ['name', 'sortname']


class StateListView(ListModelMixin, GenericViewSet):
    queryset = State.objects.all()
    serializer_class = StateSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['name']


class CityListView(ListModelMixin, GenericViewSet):
    serializer_class = CitySerializer
    queryset = City.objects.all()
    filter_backends = [filters.SearchFilter]
    search_fields = ['name']
