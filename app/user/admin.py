from django.contrib import admin, messages
from django.contrib.auth.admin import UserAdmin
from django.http import HttpResponseRedirect
from django.utils.translation import ngettext
from django_admin_listfilter_dropdown.filters import RelatedDropdownFilter
from rangefilter.filters import DateRangeFilter
from django.urls import path

from user.models import User


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    # change_list_template = "entities/heroes_changelist.html"
    list_display = (
        "email", "first_name", "last_name", "is_verified", "is_active",
    )
    fieldsets = None
    fields = (
        "first_name", "last_name", "email", "avatar", "country", "state", "city", "date_of_birth", "is_verified",
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "email", "password1", "password2", "first_name", "last_name", "avatar", "country", "state", "city",
                    "date_of_birth",
                ),
            },
        ),
    )
    search_fields = (
        "first_name", "last_name", "email",
    )
    list_filter = (
        ("country", RelatedDropdownFilter), ("date_of_birth", DateRangeFilter), "is_active", "is_verified",
    )
    # TODO make for month
    # date_hierarchy = 'date_of_birth'
    ordering = ("email",)
    actions = ("mark_user_deactivated", "mark_user_activated",)

    @admin.action(description='Mark selected user deactivated')
    def mark_user_deactivated(self, request, queryset):
        updated = queryset.update(is_active=False)
        self.message_user(request, ngettext(
            '%d user was successfully marked as deactivated.',
            '%d users were successfully marked as deactivated.',
            updated,
        ) % updated, messages.SUCCESS)

    @admin.action(description='Mark selected user activated')
    def mark_user_activated(self, request, queryset):
        updated = queryset.update(is_active=True)
        self.message_user(request, ngettext(
            '%d user was successfully marked as activated.',
            '%d users were successfully marked as activated.',
            updated,
        ) % updated, messages.SUCCESS)

    def response_change(self, request, obj):
        if "make-deactivate" in request.POST:
            obj.is_active = False
            obj.save()
        return super().response_change(request, obj)

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('activate/<int:id>/', self.user_activate, name='activate')
        ]
        return my_urls + urls

    def user_activate(self, request, id):
        user = self.model.objects.get(id=id)
        user.is_active = True
        user.save()
        self.message_user(request, f'User {id} activate SUCCESSFULLY !!!')
        return HttpResponseRedirect("/admin/user/user/")


