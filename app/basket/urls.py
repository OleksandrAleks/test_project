from django.urls import path, include
from rest_framework import routers
from .views import CartItemsViewSet


app_name = 'cart'

router = routers.SimpleRouter()
router.register(r'cart_item', CartItemsViewSet, basename='cart_item')

urlpatterns = [
    path('', include(router.urls)),
]
