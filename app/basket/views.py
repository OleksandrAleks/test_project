from django.contrib.auth import get_user_model
from django.db.models import F
from drf_spectacular.utils import extend_schema
from rest_framework import permissions
from rest_framework.decorators import action
from rest_framework.mixins import CreateModelMixin, DestroyModelMixin, ListModelMixin, UpdateModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from .models import CartItem
from .serializers import CartSerializer, CreateCartItemSerializer, ListCartItemSerializer, UpdateCartItemSerializer

User = get_user_model()


class CartItemsViewSet(ListModelMixin, CreateModelMixin, UpdateModelMixin, DestroyModelMixin, GenericViewSet):
    queryset = CartItem.objects.all()
    permission_classes = [permissions.IsAuthenticated]
    http_method_names = ['post', 'patch', 'delete', 'get']

    def get_serializer_class(self):
        if self.action == 'create':
            return CreateCartItemSerializer
        if self.action == 'partial_update':
            return UpdateCartItemSerializer
        if self.action == 'list':
            return ListCartItemSerializer
        if self.action == 'cart':
            return CartSerializer

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.action == 'list':
            queryset = self.request.user.cart_items.annotate(tot_price=F('quantity') * F('product__price'))
            return queryset
        return queryset

    @extend_schema()
    @action(methods=['get'], detail=False, permission_classes=[permissions.IsAuthenticated])
    def cart(self, request, *args, **kwargs):
        owner = self.request.user
        queryset = User.objects.filter(pk=owner.id)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
