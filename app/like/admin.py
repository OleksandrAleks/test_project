from django.contrib import admin
from .models import LikeList


@admin.register(LikeList)
class AdminLikeList(admin.ModelAdmin):
    filter_horizontal = ['product']
