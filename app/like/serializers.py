from rest_framework import serializers

from .models import LikeList


class LikeListSerializer(serializers.ModelSerializer):
    total_price = serializers.FloatField(source='tot_price')

    class Meta:
        model = LikeList
        fields = ('title', 'created_at', 'updated_at', 'product', 'is_default', 'total_price')


class CreateLikeListSerializer(serializers.ModelSerializer):

    class Meta:
        model = LikeList
        fields = ('title',)

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data.setdefault('user', user)
        like_list_obj = LikeList.objects.create(**validated_data)
        return like_list_obj


class UpdateLikeListSerializer(serializers.ModelSerializer):

    class Meta:
        model = LikeList
        fields = ('product',)

    def update(self, instance, validated_data):
        products = validated_data.get('product', None)
        if LikeList.objects.filter(product__in=products).exists():
            like_list = LikeList.objects.filter(product__in=products)
            [item.product.remove(*products) for item in like_list]
            instance.product.add(*products)
            instance.save()
        return instance


class AddLikeListSerializer(serializers.ModelSerializer):

    class Meta:
        model = LikeList
        fields = ('product',)
