from django.db import models
from django.contrib.auth import get_user_model
from store.models import Product

User = get_user_model()


class LikeList(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='likes')
    title = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    product = models.ManyToManyField(Product)
    is_default = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'Like List'

    def __str__(self):
        return f'{self.title}'
