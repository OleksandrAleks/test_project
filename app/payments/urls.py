from django.urls import path, include
from .views import home, stripe_webhook, StripeIntentView

app_name = 'payments'

urlpatterns = [
    # path('checkout-session/', CreateCheckoutSessionView.as_view(), name='checkout_session'),
    path('home/', home, name='home'),
    # path('checkout/', checkout, name='checkout'),
    path('stripe_webhook/', stripe_webhook, name='stripe_webhook'),
    path('create-payment-intent/', StripeIntentView.as_view(), name='create-payment-intent'),
]
