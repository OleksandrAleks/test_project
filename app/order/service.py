import requests

from app import settings


class NovaPostaService:

    URL = 'https://api.novaposhta.ua/v2.0/json/'
    API_KEY = settings.NOVA_POSTA_API_KEY

    def data_by_locality(self, method, locality,  **kwargs):
        params = {
            'apiKey': self.API_KEY,
            "modelName": "Address",
            "calledMethod": method,
            "methodProperties": locality
        }
        params.update(kwargs)
        return requests.post(self.URL, json=params)

    def nova_posta_locality(self, locality, **kwargs):
        return self.data_by_locality("getCities", {"FindByString": locality}, **kwargs)

    def nova_posta_office(self, locality, **kwargs):
        return self.data_by_locality('getWarehouses', {"CityName": locality}, **kwargs)
