from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import extend_schema, OpenApiParameter
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import CreateModelMixin, ListModelMixin, RetrieveModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from store.models import Product
from .models import Order, OrderItem
from .serializers import NovaPostaLocalitySerializer, NovaPostaOfficeSerializer, OrderSerializer, LocalitySerializer
from .service import NovaPostaService
from django.core.cache import cache


class OrderViewSet(CreateModelMixin, RetrieveModelMixin, ListModelMixin, GenericViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = self.queryset
        if self.action == 'list':
            queryset = queryset.filter(buyer=self.request.user)
            return queryset
        return queryset

    def create(self, request, *args, **kwargs):
        data = self.request.data
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        user = self.request.user
        order = Order()
        order.buyer = user
        order.email = data.get('email')
        order.phone_number = data.get('phone_number')
        order.shipping_first_name = data.get('shipping_first_name')
        order.shipping_last_name = data.get('shipping_last_name')
        order.shipping_surname = data.get('shipping_surname')
        order.shipping_city = data.get('shipping_city')
        # TODO reneme field
        order.delivery = data.get('delivery')
        # TODO castom field
        order.total_price = sum([item.quantity * item.product.price for item in user.cart_items.all()])
        order.save()

        order_items = user.cart_items.all()
        for item in order_items:
            OrderItem.objects.create(
                order=order,
                product=item.product,
                quantity=item.quantity,
                price=item.product.price
            )
            item_product = Product.objects.filter(id=item.product.id).first()
            item_product.quantity = item_product.quantity - item.quantity
            item_product.save()

        user.cart_items.all().delete()
        return Response(serializer.data)


class NovaPostaOfficeView(GenericAPIView):
    serializer_class = NovaPostaOfficeSerializer

    locality = OpenApiParameter(
        name='locality',
        type=OpenApiTypes.STR,
        location=OpenApiParameter.QUERY,
        description='Locality nova posta office'
    )

    @extend_schema(parameters=[locality])
    def get(self, request):
        locality = self.request.query_params.get('locality', '').lower()
        data = cache.get(locality, None)
        if not data:
            service = NovaPostaService()
            resp = service.nova_posta_office(locality=locality)
            resp_data = resp.json()['data']
            serializer = self.get_serializer(data=resp_data, many=True)
            serializer.is_valid()
            data = serializer.data
            cache.set(locality, data, 60 * 60)  # Set time life cache min * sec
        return Response(data)


class NovaPostaLocalityView(GenericAPIView):
    serializer_class = NovaPostaLocalitySerializer

    locality = OpenApiParameter(
        name='locality',
        type=OpenApiTypes.STR,
        location=OpenApiParameter.QUERY,
        description='Locality where there is a delivery nova posta'
    )

    @extend_schema(parameters=[locality])
    def get(self, request):
        serial = LocalitySerializer(data=self.request.query_params)
        serial.is_valid(raise_exception=True)
        locality = serial.data.get('locality').lower()
        data = cache.get('loc_' + locality, None)
        if not data:
            service = NovaPostaService()
            resp = service.nova_posta_locality(locality=locality)
            resp_data = resp.json()['data']
            serializer = self.get_serializer(data=resp_data, many=True)
            serializer.is_valid()
            data = serializer.data
            cache.set('loc_' + locality, data, 60 * 60)  # Set time life cache min * sec
        return Response(data)
