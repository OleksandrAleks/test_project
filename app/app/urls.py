from django.contrib import admin
from django.urls import path, include, re_path
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path("admin/", admin.site.urls),
    re_path(r"^chaining/", include("smart_selects.urls")),
    path("user/", include("user.urls")),
    path("store/", include("store.urls")),
    path("cart/", include("basket.urls")),
    path("like/", include("like.urls")),
    path("order/", include("order.urls")),
    path("comment/", include("comment.urls")),
    path("payments/", include("payments.urls")),
    path("schema/", SpectacularAPIView.as_view(), name="schema"),
    path("swagger/", SpectacularSwaggerView.as_view(url_name="schema"), name="swagger"),
    path('__debug__/', include('debug_toolbar.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
