from django_filters.rest_framework import DjangoFilterBackend
from drf_spectacular.utils import extend_schema
from drf_spectacular.views import OpenApiParameter, OpenApiTypes
from rest_framework import permissions, status
from rest_framework.mixins import RetrieveModelMixin, ListModelMixin, CreateModelMixin, UpdateModelMixin, DestroyModelMixin
from rest_framework.viewsets import GenericViewSet, ModelViewSet

from .filters import ProductFilter
from .models import Category, Product
from .serializers import CategorySerializers, ParentCategorySerializers, ProductSetSerializers, ComparedListSerializer, RootCategorySerializer
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView


class CategoryViewSet(RetrieveModelMixin, GenericViewSet):
    queryset = Category.objects.all()
    # permission_classes = [permissions.IsAuthenticated]
    serializer_class = ParentCategorySerializers


class ProductViewSet(ListModelMixin, RetrieveModelMixin, CreateModelMixin, GenericViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSetSerializers
    filter_backends = [DjangoFilterBackend]
    filterset_class = ProductFilter
    ordering_fields = ['price', 'created']
    ordering = ['-created']

    def get_permissions(self):

        if self.action == 'create':
            # permission_classes = [permissions.IsAdminUser]
            permission_classes = []
        else:
            # permission_classes = [permissions.IsAuthenticated]
            permission_classes = []
        return [permission() for permission in permission_classes]
    
    # def get_queryset(self):
    #     queryset = super().get_queryset()
    #     if self.action == 'list':
    #         query_params = self.request.query_params
    #         params_value = {item[0]: item[1] for item in query_params.items() if item[0].isdigit()}
    #         value_param = params_value.values()
    #         if value_param:
    #             queryset = queryset.filter(category_param_value__value__in=value_param)
    #             return queryset
    #     return queryset

    params = OpenApiParameter(
        name='params',
        type=OpenApiTypes.OBJECT,
        location=OpenApiParameter.QUERY,
        description='Params category example: "id: value"'
    )

    @extend_schema(parameters=[params, ])
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    id_product = OpenApiParameter(
        name='id_product',
        type=OpenApiTypes.OBJECT,
        location=OpenApiParameter.QUERY,
        description='Products id for comparison'
    )

    @extend_schema(parameters=[id_product, ])
    @action(methods=['get'], detail=False, serializer_class=ComparedListSerializer,)
    def comparison(self, request, *args, **kwargs):
        product_id_list = self.request.query_params.values()
        queryset = Product.objects.filter(id__in=product_id_list)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class CategoryParamsViewSet(RetrieveModelMixin, ListModelMixin, GenericViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializers

    def get_serializer_class(self):
        if self.action == 'list':
            return RootCategorySerializer
        elif self.action == 'retrieve':
            return CategorySerializers

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.action == 'list':
            queryset = Category.objects.filter(parent=None)
        return queryset


class ComparedViewSet(ListModelMixin, DestroyModelMixin, GenericViewSet):
    queryset = Product.objects.all()
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = ComparedListSerializer

    def get_queryset(self):
        if self.action == 'list':
            queryset = self.request.user.product_set.all()[:5]
            return queryset
        return Product.objects.all()

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.user_comparison.remove(self.request.user)
        return Response(status=status.HTTP_204_NO_CONTENT)

    @extend_schema()
    @action(methods=['post'], detail=True, serializer_class=[], permission_classes=[permissions.IsAuthenticated])
    def add_to_comparison(self, request,  *args, **kwargs):
        product_pk = self.request.parser_context['kwargs']['pk']
        user = self.request.user
        if Product.objects.filter(pk=product_pk).exists():
            product = Product.objects.get(pk=product_pk)
            product.user_comparison.add(user)
            return Response(status=status.HTTP_200_OK)
        return Response({"Message": f"Product id = {product_pk} not found"}, status=status.HTTP_400_BAD_REQUEST)

    @extend_schema()
    @action(methods=['delete'], detail=False, permission_classes=[permissions.IsAuthenticated])
    def remove_all(self, request, *args, **kwargs):
        instances = self.request.user.product_set
        instances.clear()
        return Response(status=status.HTTP_204_NO_CONTENT)


class TestCategoryViewSet(RetrieveModelMixin, GenericViewSet):
    queryset = Category.objects.all()
    serializer_class = RootCategorySerializer