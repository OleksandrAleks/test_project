from django.core.exceptions import ObjectDoesNotExist

from .models import Category, CategoryParams, CategoryParamsValue, Product, ImageProduct
from rest_framework import serializers
from order.models import OrderItem
from django.db.models import Avg


class CreatableSlugRelatedField(serializers.SlugRelatedField):

    def to_internal_value(self, data):
        try:
            return self.get_queryset().get(**{self.slug_field: data})
        except ObjectDoesNotExist:
            return self.get_queryset().create(**{self.slug_field: data})
        except (TypeError, ValueError):
            self.fail('invalid')


class CategoryParamsValueSerializers(serializers.ModelSerializer):
    param = CreatableSlugRelatedField(
        queryset=CategoryParams.objects.all(),
        source='category_params',
        slug_field='name'
    )
    value = serializers.CharField(max_length=200)

    class Meta:

        model = CategoryParamsValue
        fields = ('param', 'value')


class ProductImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = ImageProduct
        fields = ['id', 'image']


class ProductSetSerializers(serializers.ModelSerializer):
    params = CategoryParamsValueSerializers(source='category_param_value', many=True)
    can_review = serializers.SerializerMethodField()
    count_review = serializers.SerializerMethodField()
    avg_rating = serializers.SerializerMethodField()
    images = ProductImageSerializer(many=True)

    class Meta:

        model = Product
        fields = (
            'id', 'title', 'description', 'is_active', 'price', 'quantity', 'category', 'params', 'can_review',
            'count_review', 'avg_rating', 'images'
        )

    def create(self, validated_data):
        category_param_value_data = validated_data.pop('category_param_value')
        product_obj = Product.objects.create(**validated_data)

        for param_value in category_param_value_data:
            value = param_value['value']
            if not CategoryParamsValue.objects.filter(value=value).exists():
                value_object = CategoryParamsValue.objects.create(**param_value)
                product_obj.category_param_value.add(value_object)
            else:
                value_object = CategoryParamsValue.objects.get(value=value)
                product_obj.category_param_value.add(value_object)
        return product_obj

    # TODO rewrite to annotate in queryset
    def get_can_review(self, obj):
        user = self.context['request'].user
        res = False
        if user.is_authenticated:
            res = OrderItem.objects.filter(product=obj, order__buyer=user).exists()
        return res

    def get_count_review(self, obj):
        return obj.reviews.count()

    def get_avg_rating(self, obj):
        average = obj.reviews.filter(rating__isnull=False).aggregate(average=Avg('rating'))
        return average['average']


class ParentCategorySerializers(serializers.ModelSerializer):
    child = serializers.SerializerMethodField()

    class Meta:
        model = Category
        fields = ('id', 'name', 'child')

    def get_child(self, obj):
        return bool(obj.get_children())


class ParamsValueSerializers(serializers.ModelSerializer):

    class Meta:
        model = CategoryParamsValue
        fields = ('id', 'value')


class CategoryParamsSetSerializers(serializers.ModelSerializer):
    param_value = ParamsValueSerializers(source='cat_params_value', many=True)

    class Meta:
        model = CategoryParams
        fields = ('id', 'name', 'param_value')


class CategorySerializers(serializers.ModelSerializer):
    cat_params = CategoryParamsSetSerializers(source='category_params', many=True)
    parent_params = serializers.SerializerMethodField()

    class Meta:
        model = Category
        fields = ('name', 'cat_params', 'parent_params')

    def get_parent_params(self, obj):
        serializer = CategorySerializers(instance=obj.parent)
        return serializer.data


class ComparedListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = '__all__'




class ChildSerializer(serializers.Serializer):

    def to_representation(self, instance):
        serializer = self.parent.parent.__class__(instance, context=self.context)
        return serializer.data


class RootCategorySerializer(serializers.ModelSerializer):
    children = ChildSerializer(many=True, read_only=True)

    class Meta:
        model = Category
        fields = ('id', 'name', 'parent', 'children')


# class TestCategorySerializer(serializers.Mode